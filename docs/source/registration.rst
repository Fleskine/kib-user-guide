Creating an Account
===================
On visiting the landing page, new users who wish to register may follow the following steps:

#. Select the '*Get Started*' option, situated in the top right of the navigation bar.

    ..  figure::  ../../images/GetitngStarted.png
        :class: with-border
        :alt: Getting started
        :scale: 80
        :align: center

        *Figure 1: Getting started*

#. Select the '*Create an account*' option to render the registration form and fill in the required fields correctly.

    ..  figure::  ../../images/CreateAnAcc.png
        :class: with-border
        :alt: Create account
        :scale: 80
        :align: center

        *Figure 2: Create an account*

#. Once all input fields have been populated as expected, click the '*Create*' button located at the bottom of the registration form. 

    ..  figure::  ../../images/RegFields.png
        :class: with-border
        :alt: Create account
        :scale: 80
        :align: center

        *Figure 3: Registration input fields*

#. Upon successful creation of the user account, a confirmation email will be sent to the email used for creating the account. Follow the link provided in the confirmation email to activate the newly created account.
  
    ..  figure::  ../../images/RegSuccess.png
        :class: with-border
        :alt: Create account
        :scale: 80
        :align: center

        *Figure 4: Registration success*
