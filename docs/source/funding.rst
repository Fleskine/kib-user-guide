Funding
=======
Funding opportunities
---------------------
Investors can navigate through innovations that require funding on KIB through the Funding/Support option at the top.

    ..  figure::  ../../images/FundingOpportunities.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 33: Funding opportunities*

Responding to funding requests
------------------------------
Choosing a specific innovation from the list of those requesting for funding will yield more information about it. This also includes an option for potential investors to respond to the call requests made by the innovators as shown.

    ..  figure::  ../../images/FundingRequest.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 34: Initiating a response to a funding request*

This provides a window that allows the potential investor to direct relevant questions towards the innovators seeking funding.

    ..  figure::  ../../images/FundReqResp.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 35: Sample response to a funding request*

On submission, the intended innovator will receive a notification of the investor's response as depicted.
   
    ..  figure::  ../../images/FundResp1.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 36: Funding response message (a)*

    ..  figure::  ../../images/FundResp2.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 36: Funding response message (b)*

    ..  figure::  ../../images/FundResp3.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 36: Funding response message (c)*