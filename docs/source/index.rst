.. KIB User Guide documentation master file, created by
   sphinx-quickstart on Tue Jul 11 15:16:41 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KIB User Guide documentation!
========================================
Kenya Innovation Bridge helps in the discoverability of innovations in Kenya. 
The platform is a marketplace that enables innovators, inventors, researchers 
and startups to introduce their solutions to partners, funders, customers and users.
The goal is to get as many innovations as possible to scale by attracting appropriate funding and partnerships.
This user guide highlights the basic operations that can be performed by end users as they
interact with the system.

This user manual describes the modules that each group of end users have access to. The two main
categorizations of end users are the innovators and the investors. Generally, innovators post their 
innovations on the platform while investors keep track of these innovations; and/or make investment 
offers to the innovators. This guide describes the level of access that they have to the system; 
and their interaction with it.



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   registration
   getting_started
   innovations
   investor
   funding
   institutions
   connections
   social




   
