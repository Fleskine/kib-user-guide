Investors
=========
Making an offer
---------------
This can be done as either an individual or a firm. The following steps should be followed to make an offer.

#. Navigate to the innovations page and select the innovation of interest.

#. Select the option - 'Make an Offer'.

    ..  figure::  ../../images/MakeOffer.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 26: Making an offer*

#. Provide details about the offer by filling in the form rendered.

    ..  figure::  ../../images/MakeOfferDetails.png
        :class: with-border
        :alt: Specifying offer details
        :scale: 80
        :align: center

        *Figure 27: Outlining offer details*

#. Complete the offer details by ensuring that the publicizing checkbox is enabled then submit.

    ..  figure::  ../../images/SubmitOffer2.png
        :class: with-border
        :alt: Specifying offer details
        :scale: 80
        :align: center

        *Figure 28: Submitting offer details*


Investor Hub
------------
This section of the application provides investors with quick summary details that are crucial to them. These include:

* Innovation rankings
* Recommendations.
* Recent Deals.
* Saved innovations.
* Saved notes.

    ..  figure::  ../../images/InvestorsHub1.png
        :class: with-border
        :alt: Specifying offer details
        :scale: 80
        :align: center

        *Figure 29(a): Investor hub*


    ..  figure::  ../../images/InvestorsHub2.png
        :class: with-border
        :alt: Specifying offer details
        :scale: 80
        :align: center

        *Figure 29(b): Investor hub*


Investor Profiles
-----------------
These provide information about specific investors. To access an investor profile, navigate to the investors' page and select any of them to reveal their profile.

    ..  figure::  ../../images/InvestorsTab.png
        :class: with-border
        :alt: Specifying offer details
        :scale: 80
        :align: center

        *Figure 30: Investors*

    ..  figure::  ../../images/InvestorProfiles.png
        :class: with-border
        :alt: Specifying offer details
        :scale: 80
        :align: center

        *Figure 31: Investor profiles*

    ..  figure::  ../../images/InvestorProfile1.png
        :class: with-border
        :alt: Specifying offer details
        :scale: 80
        :align: center

        *Figure 32(a): Specific investor profile*

    ..  figure::  ../../images/InvestorProfile2.png
        :class: with-border
        :alt: Specifying offer details
        :scale: 80
        :align: center

        *Figure 32(b): Specific investor profile*