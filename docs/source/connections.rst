Connections
===========
KIB permits users to connect with each other, serving as a networking platform. To connect with innovators:


Connection requests
-------------------

#. Visit the 'Innovations' page.

#. Select an innovation of preference.

#. Select the icon below the 'Connect with Team' option.

    ..  figure::  ../../images/ConnectTeam.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 42: Connection request option*

#. Click connect in the mini-window.

    ..  figure::  ../../images/ConnectTeam2.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 43: Sending connection request*

Once the request has been sent to the innovator, they will receive a notification and may choose to accept, reject or block the request.

    ..  figure::  ../../images/ReceiveConnReq.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 44: Options to a connection request*


Messaging connections
---------------------

Accepting a connection request would permit subsequent sharing of messages between the connected users.

    ..  figure::  ../../images/MessageOption.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 45: Messaging option*

Users can then compose and send messages as shown.
    ..  figure::  ../../images/ComposeMessage.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 46: Composing a message*

The intended recipient will receive the message once it has been sent.

    ..  figure::  ../../images/MessageArrived.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 47: Message notification*