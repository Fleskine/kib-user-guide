Social Posts
============

Viewing posts
-------------
KIB enables users to share posts just like social platforms do. These are available to the user by navigating to the 'Home' section.

    ..  figure::  ../../images/HomeSection.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 48: Sample posts*

Creating posts
--------------
To create a post, click the 'Compose' button and include the message and an image if available.

    ..  figure::  ../../images/ComposePost.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 49: Composing a post*


Commenting
----------
Users can share comments in response to the social posts as below.

    ..  figure::  ../../images/Commenting.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 50: Commenting under a post*