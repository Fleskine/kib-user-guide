Innovations
===========

Creating an innovation
----------------------

To create an innovation:

#. Select the "Create Innovation" option towards the top right to render an input for provision of details pertaining to the innovation.
 
    ..  figure::  ../../images/InnovCreate.png
        :class: with-border
        :alt: Option to create an innovation
        :scale: 80
        :align: center

        *Figure 19: Innovation creation option*

#. Populate the fields with the required details. These include:

   * Innovation title, tagline and year.
   * Intellectual Property(IP) Type.
   * Stage of the innovation and the county headquarters.
   * Description of the innovation.
   * What the innovation seeks to attract eg. Funding.
   * Economic sector the innovation lies in.
   * Sustainable Development Goals(SDG) to be addressed by the innovation.
   * Affiliated institutions and/or organizations (if any).


#. Click the "Create" button at the bottom of the form to finalize. If all details were keyed in to satisfaction, a success message will be rendered at the bottom right with a green dialog box. The user will receive a notification if the innovation goes live upon approval by the system administrator.



Types of innovations
--------------------
By default, navigating to the innovation page renders available innovations based on three categories that include:

#. Interest-based innovations.

    ..  figure::  ../../images/InterestInnovations.png
        :class: with-border
        :alt: Interest-based innovations
        :scale: 80
        :align: center

        *Figure 20: Interest-based innovations*

#. Recent innovations.

    ..  figure::  ../../images/PopularInnovations.png
        :class: with-border
        :alt: Popular innovations
        :scale: 80
        :align: center

        *Figure 21: Popular innovations*

#. Popular innovations.

    ..  figure::  ../../images/RecentInnovations.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 22: Recent innovations*

#. Recommended for you (based on peer-recommendations).


Innovation details
------------------
To retrieve information pertaining to a specific Innovation, click the 'View' button below it. 
 
    ..  figure::  ../../images/InnovImg.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 23: Specific innovation*

The lower section contains details like the overview, affiliated institutions and investors/investments linked to it.

    ..  figure::  ../../images/InnovOverview.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 24: Innovation overview*


Innovation ratings
------------------
To rate an innovation, scroll to the lower section of the specific innovation and use the stars to give a rating.

    ..  figure::  ../../images/InnovRate.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 25: Innovation rating*