Getting Started
===============

Innovator
---------

Having successfully created and activated a user account:

#. Log in by providing your email and password as shown.
 
    ..  figure::  ../../images/LoginForm.png
        :class: with-border
        :alt: Log in
        :scale: 80
        :align: center

        *Figure 5: Login form*

#. Select your preferred role and interests. Specifying interests aids in finding matches for innovations.

    ..  figure::  ../../images/RoleSelect.png
        :class: with-border
        :alt: Role selection
        :scale: 80
        :align: center

        *Figure 6: Role selection*

    ..  figure::  ../../images/Interests.png
        :class: with-border
        :alt: Interests
        :scale: 80
        :align: center

        *Figure 7: Exploration interests*

    ..  figure::  ../../images/InterestSectors.png
        :class: with-border
        :alt: Sectors of interest
        :scale: 80
        :align: center

        *Figure 8: Sectors of interest*

    ..  figure::  ../../images/SDGs.png
        :class: with-border
        :alt: SDGs of interest
        :scale: 80
        :align: center

        *Figure 9: SDGs of interest*

#. Complete the process by clicking the "*Let's go*" button at the bottom. This will render a page of innovations based on the interests outlined, popular innovations as at that moment and the latest innovations.
   
    ..  figure::  ../../images/InterestInnovations.png
        :class: with-border
        :alt: Interest-based innovations
        :scale: 80
        :align: center

        *Figure 10: Interest-based innovations*
           
    ..  figure::  ../../images/PopularInnovations.png
        :class: with-border
        :alt: Popular innovations
        :scale: 80
        :align: center

        *Figure 11: Popular innovations*
           
    ..  figure::  ../../images/RecentInnovations.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 12: Recent innovations*

Clicking the "View More" button in the top right grants the user access to more innovations and permits filtering based on counties.


Investor
--------

Having successfully created and activated a user account:

#. Specify the type of investor - whether an individual or a firm.

    ..  figure::  ../../images/InvestorType.png
        :class: with-border
        :alt: Investor type
        :scale: 80
        :align: center

        *Figure 13: Specifying investor type*

   * If an individual investor, select one of the four categories as depicted.

    ..  figure::  ../../images/IndividualInvestortypes.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 14: Types of individual investors*

   * If an investment firm, select either an existing firm to join or create a new one.

    ..  figure::  ../../images/ExistingInvestmentFirm.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 15: Selecting an existing investment firm*

    ..  figure::  ../../images/NewInvestmentFirm.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 16: Creating a new investment firm*

    * On successful creation of the investor account, a notification will be rendered to inform the user of this action. Below are illustrations of confirmatory notifications for both individual and firm investors.

    ..  figure::  ../../images/IndividualInvestorSuccess.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 17: Individual investor success message*

    ..  figure::  ../../images/InvestorFirmSuccess.png
        :class: with-border
        :alt: Recent innovations
        :scale: 80
        :align: center

        *Figure 18: Investor firm success message*