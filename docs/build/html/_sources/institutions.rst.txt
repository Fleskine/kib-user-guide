Institutions
============

List of institutions
--------------------

To view the list of member institutions on KIB, click the 'Institutions' option at the top as depicted.

    ..  figure::  ../../images/InstitutionsTab.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 37: Institutions*

This renders a page with the various member institutions as shown.

    ..  figure::  ../../images/InstitutionList.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 38: List of institutions*


Affiliated users
----------------
To display the list of users linked to an institution, select the 'Users' tab as illustrated.

    ..  figure::  ../../images/LinkedUsers.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 39: Linked users*


Affiliated institutions
-----------------------
To display the list of other institutions that are linked to an institution, select the 'Affiliated Institutions' tab as illustrated.

    ..  figure::  ../../images/AffiliatedInstitutions.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center

        *Figure 40: Linked institutions*

Events
------
To display dates for events scheduled to take place in institutions, select the 'Events' tab as shown.

    ..  figure::  ../../images/InstitutionDates.png
        :class: with-border
        :alt: Making an offer
        :scale: 80
        :align: center
        

        *Figure 41: Events*